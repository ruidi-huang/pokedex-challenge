import PropTypes from 'prop-types';
import moment from 'moment';

export const PokemonPropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  order: PropTypes.number.isRequired,
  weight: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  types: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.shape({
        name: PropTypes.string.isRequired
      }).isRequired
    })
  ).isRequired,
  stats: PropTypes.arrayOf(
    PropTypes.shape({
      base_stat: PropTypes.number.isRequired,
      stat: PropTypes.shape({
        name: PropTypes.string.isRequired
      }).isRequired
    })
  ).isRequired,
  sprites: PropTypes.shape({
    other: PropTypes.shape({
      'official-artwork': PropTypes.shape({
        front_default: PropTypes.string.isRequired
      }).isRequired
    }).isRequired
  }).isRequired
});

export const CapturedPokemonPropType = PropTypes.shape({
  nickname: PropTypes.string,
  capturedDate: PropTypes.string.isRequired,
  capturedLevel: PropTypes.string.isRequired,
  pokemonId: PropTypes.number.isRequired
});

export function validateCapturedPokemon(capturedPokemon) {
  const errors = {};
  if (
    !capturedPokemon.capturedDate ||
    !/^\d{4}-\d{2}-\d{2}$/.test(capturedPokemon.capturedDate)
  ) {
    errors.capturedDate = true;
  } else {
    const capturedDate = moment(capturedPokemon.capturedDate, 'YYYY-MM-DD').startOf('day');
    if (
      !capturedDate.isValid() ||
      capturedDate.isAfter(moment())
    ) {
      errors.capturedDate = true;
    }
  }
  if (
    !capturedPokemon.capturedLevel ||
    !/^\d{1,3}$/.test(capturedPokemon.capturedLevel) ||
    capturedPokemon.capturedLevel.startsWith('0')
  ) {
    errors.capturedLevel = true;
  } else {
    const capturedLevel = parseInt(capturedPokemon.capturedLevel);
    if (capturedLevel > 100) {
      errors.capturedLevel = true;
    }
  }
  return errors;
}
