import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import Button from 'lib/components/inputs/button/Button';
import TextInput from 'lib/components/inputs/text/TextInput';
import { PokemonPropType, validateCapturedPokemon } from 'app/components/pokemon';

import styles from './pokemon-capture-form.scss';

const PokemonCaptureForm = (props) => {
  const [capturedPokemon, setCapturedPokemon] = useState({
    nickname: null,
    capturedDate: moment().format('YYYY-MM-DD'),
    capturedLevel: null,
    pokemonId: props.pokemon.id
  });

  function updateCapturedPokemon(values) {
    setCapturedPokemon({
      ...capturedPokemon,
      ...values
    });
  }

  const errors = validateCapturedPokemon(capturedPokemon);
  return (
    <div className={ styles.pokemonCaptureForm }>
      <div className={ styles.title }>
        { `Capturing ${_.capitalize(props.pokemon.name)}` }
      </div>
      <div className={ styles.fields }>
        <TextInput
          grow
          placeholder={ 'Nickname' }
          value={ capturedPokemon.nickname }
          onChange={ (nickname) => updateCapturedPokemon({ nickname }) }
        />
        <TextInput
          grow
          placeholder={ 'Captured Date' }
          error={ Boolean(capturedPokemon.capturedDate && errors.capturedDate) }
          value={ capturedPokemon.capturedDate }
          onChange={ (capturedDate) => updateCapturedPokemon({ capturedDate }) }
        />
        <TextInput
          grow
          placeholder={ 'Captured Level' }
          error={ Boolean(capturedPokemon.capturedLevel && errors.capturedLevel) }
          value={ capturedPokemon.capturedLevel }
          onChange={ (capturedLevel) => updateCapturedPokemon({ capturedLevel }) }
        />
      </div>
      <Button
        grow
        primary
        disabled={ _.some(errors) }
        onClick={ props.onCapture && (() => props.onCapture(capturedPokemon)) }
      >
        { 'Capture' }
      </Button>
    </div>
  );
};

PokemonCaptureForm.propTypes = {
  pokemon: PokemonPropType.isRequired,
  onCapture: PropTypes.func
};

export default PokemonCaptureForm;
