import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { ANIMATION_DELAY } from 'app/components/App';
import { CapturedPokemonPropType } from 'app/components/pokemon';
import CapturedPokemonRow from 'app/components/pokemon/CapturedPokemonRow';

import styles from './captured-pokemon-grid.scss';
import { releasePokemon, saveCapturedPokemons } from 'app/store/actions/capturedPokemons';

const CapturedPokemonGrid = (props) => {
  const [hide, setHide] = useState(props.delay);

  function releasePokemon(capturedPokemon) {
    props.releasePokemon(capturedPokemon);
    props.saveCapturedPokemons();
  }
  useEffect(() => {
    _.delay(() => {
      setHide(false);
    }, ANIMATION_DELAY);
  }, []);

  if (hide) {
    return null;
  }

  return (
    <div className={ styles.capturedPokemonGrid }>
      <div
        className={ styles.header }
      >
        <div
          className={ styles.pokemon }
        >
          { 'Pokemon' }
        </div>
        <div
          className={ styles.nickname }
        >
          { 'Nickname' }
        </div>
        <div
          className={ styles.capturedDate }
        >
          { 'Captured On' }
        </div>
        <div
          className={ styles.capturedLevel }
        >
          { 'Captured Level' }
        </div>
      </div>
      {
        _.map(props.capturedPokemons, (capturedPokemon, index) => (
          <CapturedPokemonRow
            key={ index }
            capturedPokemon={ capturedPokemon }
            onRelease={ releasePokemon }
          />
        ))
      }
    </div>
  );
};

CapturedPokemonGrid.propTypes = {
  delay: PropTypes.bool,
  releasePokemon: PropTypes.func.isRequired,
  saveCapturedPokemons: PropTypes.func.isRequired,
  capturedPokemons: PropTypes.arrayOf(
    CapturedPokemonPropType
  ).isRequired
};

export default connect(
  (state) => ({
    loading: state.pokemons.loading,
    pokemons: state.pokemons.all
  }),
  {
    releasePokemon,
    saveCapturedPokemons
  }
) (CapturedPokemonGrid);
