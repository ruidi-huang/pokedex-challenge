import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import FormattedDate from 'lib/components/wrappers/datetime/FormattedDate';

import { CapturedPokemonPropType } from 'app/components/pokemon';
import PokemonName from 'app/components/pokemon/PokemonName';
import Button from 'lib/components/inputs/button/Button';
import styles from './captured-pokemon-grid.scss';

const CapturedPokemonRow = (props) => {
  const pokemon = props.pokemonsById[props.capturedPokemon.pokemonId];
  console.log(props.capturedPokemon);

  return (
    <div
      className={ styles.row }
    >
      <div
        className={ styles.pokemon }
      >
        <div
          className={
            classNames(
              styles.imageBackground,
              styles[pokemon.types[0].type.name]
            )
          }
        >
          <img
            className={ styles.image }
            src={ pokemon.sprites.other['official-artwork'].front_default }
          />
        </div>
        <div className={ styles.details }>
          <div className={ styles.name }>
            <PokemonName
              pokemon={ pokemon }
            />
          </div>
          <div className={ styles.types }>
            {
              _.map(pokemon.types, ({ type }) => (
                <div
                  key={ type.name }
                  className={ styles.type }
                >
                  { _.capitalize(type.name) }
                </div>
              ))
            }
          </div>
          
          <div className={ styles.types }>
          <Button
          primary
          onClick={ () => props.onRelease(props.capturedPokemon) }
          >
            { 'Release' }
          </Button>
          </div>

        </div>
      </div>
      <div
        className={
          classNames(styles.nickname, {
            [styles.none]: !props.capturedPokemon.nickname
          })
        }
      >
        { props.capturedPokemon.nickname || 'None' }
      </div>
      <div
        className={ styles.capturedDate }
      >
        <FormattedDate
          date={ props.capturedPokemon.capturedDate }
        />
      </div>
      <div
        className={ styles.capturedLevel }
      >
        { props.capturedPokemon.capturedLevel }
      </div>
    </div>
  );
};

CapturedPokemonRow.propTypes = {
  pokemonsById: PropTypes.object.isRequired,
  capturedPokemon: CapturedPokemonPropType.isRequired,
  onRelease: PropTypes.func.isRequired
};

export default connect(
  (state) => ({
    pokemonsById: state.pokemons.byId
  })
)(CapturedPokemonRow);
