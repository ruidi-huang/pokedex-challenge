import _ from 'lodash';
import {
  LOAD_CAPTURED_POKEMONS,
  CAPTURE_POKEMON,
  RELEASE_POKEMON
} from 'app/store/actions/capturedPokemons';

const INITIAL_STATE = {
  all: []
};

function capturedPokemons(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOAD_CAPTURED_POKEMONS:
      return {
        all: action.capturedPokemons
      };
    case CAPTURE_POKEMON:
      return {
        all: _.concat(state.all, action.capturedPokemon)
      };
    case RELEASE_POKEMON:
      return {
        all: _.filter(state.all, (pokemon) => pokemon.pokemonId != action.releasedPokemon.pokemonId)
      };
    default:
      return state;
  }
}

export default capturedPokemons;
