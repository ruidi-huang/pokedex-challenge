export const LOAD_CAPTURED_POKEMONS = 'load-captured-pokemons';
export const CAPTURE_POKEMON = 'capture-pokemon';
export const RELEASE_POKEMON = 'release-pokemon';

export const loadCapturedPokemons = () => {
  return {
    type: LOAD_CAPTURED_POKEMONS,
    capturedPokemons: JSON.parse(
      localStorage.getItem('capturedPokemons') || '[]'
    )
  };
};

export const saveCapturedPokemons = () => {
  return (dispatch, getState) => {
    const state = getState();
    localStorage.setItem(
      'capturedPokemons',
      JSON.stringify(state.capturedPokemons.all)
    );
  };
};

export const capturePokemon = (capturedPokemon) => {
  return {
    capturedPokemon,
    type: CAPTURE_POKEMON
  };
};

export const releasePokemon = (releasedPokemon) => {
  return {
    releasedPokemon,
    type: RELEASE_POKEMON
  };
}
