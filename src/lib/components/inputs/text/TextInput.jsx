import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import styles from './text-input.scss';

const TextInput = (props) => {
  function onChange(event) {
    const value = event.target.value || null;
    if (props.onChange) {
      props.onChange(value);
    }
  }

  return (
    <input
      className={
        classNames(styles.textInput, {
          [styles.error]: props.error,
          [styles.grow]: props.grow
        })
      }
      type={ 'text' }
      placeholder={ props.placeholder }
      value={ props.value || '' }
      onChange={ onChange }
    />
  );
};

TextInput.propTypes = {
  error: PropTypes.bool,
  grow: PropTypes.bool,

  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default TextInput;
