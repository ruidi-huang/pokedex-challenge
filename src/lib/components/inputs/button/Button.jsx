import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import styles from './button.scss';

const Button = (props) => {
  return (
    <button
      className={
        classNames(styles.button, {
          [styles.bold]: props.bold,
          [styles.disabled]: props.disabled,
          [styles.error]: props.error,
          [styles.grow]: props.grow,
          [styles.hasImage]: Boolean(props.imgSrc),
          [styles.large]: props.large,
          [styles.popIn]: props.popIn,
          [styles.primary]: props.primary
        })
      }
      onClick={ props.onClick }
    >
      {
        props.imgSrc && (
          <img
            className={ styles.img }
            src={ props.imgSrc }
          />
        )
      }
      { props.children }
    </button>
  );
};

Button.propTypes = {
  bold: PropTypes.bool,
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  grow: PropTypes.bool,
  large: PropTypes.bool,
  popIn: PropTypes.bool,
  primary: PropTypes.bool,

  imgSrc: PropTypes.string,
  children: PropTypes.node,

  onClick: PropTypes.func
};

export default Button;
