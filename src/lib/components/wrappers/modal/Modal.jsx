import PropTypes from 'prop-types';
import React from 'react';

import styles from './modal.scss';

const Modal = (props) => {
  return (
    <div className={ styles.wrapper }>
      <div
        className={ styles.shade }
        onClick={ props.close }
      />
      <div className={ styles.modal }>
        { props.children }
      </div>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.node,
  close: PropTypes.func
};

export default Modal;
